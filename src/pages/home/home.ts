import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { User } from '../../interfaces/user';

import { AngularFireAuth } from 'angularfire2/auth';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public user: User


  constructor(
    public navCtrl: NavController,
    public userProvider: UserProvider,
    private afAuth: AngularFireAuth,
  ) {
    this.user = userProvider.user
  }

  logout() {
    this.afAuth.auth.signOut().then(() => {
      this.userProvider.user = {}
      this.navCtrl.setRoot(LoginPage)
    })
  }

}

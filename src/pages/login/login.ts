import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { UserProvider } from '../../providers/user/user';
import { HomePage } from '../home/home';

import { Platform } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';

import { GooglePlus } from '@ionic-native/google-plus';

import { API_CLIENT_GOOGLE } from '../../data/main';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private afAuth: AngularFireAuth,
    private facebook: Facebook,
    private platform: Platform,
    private googlePlus: GooglePlus,
    public userProvider: UserProvider,
  ) {

  }

  /**
   *
   *
   * @returns
   * @memberof LoginPage
   */
  public signInWithFacebook() {
    if (this.platform.is('cordova')) {
      this.facebook.login(['email', 'public_profile']).then(res => {
        const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
        firebase.auth().signInWithCredential(facebookCredential)
          .then((user) => this.setValueUser(user.displayName, user.email, user.photoURL, user.uid, 'facebook'))
          .catch((error) => console.log(error))
      })
    } else {
      return this.afAuth.auth
        .signInWithPopup(new firebase.auth.FacebookAuthProvider())
        .then(res => this.setValueUser(res.user.displayName, res.user.email, res.user.photoURL, res.user.uid, 'facebook'));
    }
  }


  /**
   *
   *
   * @private
   * @param {string} name
   * @param {string} email
   * @param {string} photo
   * @param {string} uid
   * @param {string} provider
   * @memberof LoginPage
   */
  private setValueUser(name: string, email: string, photo: string, uid: string, provider: string) {
    this.userProvider.getInfoUser(name, email, photo, uid, provider)
    this.navCtrl.setRoot(HomePage)
  }

  /**
   *
   *
   * @memberof LoginPage
   */
  public signInWithGoogle() {
    this.googlePlus.login({
      'webClientId': `${API_CLIENT_GOOGLE}`,
      'offline': true
    })
      .then(res => {
        console.log(res)
        firebase.auth().signInWithCredential(firebase.auth.GoogleAuthProvider.credential(res.idToken))
          .then(user => {
            console.log("Firebase success: " + user);
            this.setValueUser(user.displayName, user.email, user.photoURL, user.uid, 'google')
          })
          .catch(error => console.log("Firebase failure: " + JSON.stringify(error)));
      })
      .catch(err => console.error(err));
  }

}

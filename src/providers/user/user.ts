
import { Injectable } from '@angular/core';
import { User } from '../../interfaces/user';

@Injectable()
export class UserProvider {

  public user: User = {}

  constructor() {

  }

  /**
   *
   *
   * @param {string} name
   * @param {string} email
   * @param {string} photo
   * @param {string} uid
   * @param {string} provider
   * @memberof UserProvider
   */
  public getInfoUser(name: string, email: string, photo: string, uid: string, provider: string) {
    this.user.name = name
    this.user.email = email
    this.user.photo = photo
    this.user.uid = uid
    this.user.provider = provider
  }

}

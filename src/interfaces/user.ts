export interface User {
   name?: string;
   email?: string;
   photo?: string;
   uid?: string;
   provider?: string;
}